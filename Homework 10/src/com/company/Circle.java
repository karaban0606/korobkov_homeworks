package com.company;

public class Circle extends Ellipse implements MovingFigure{

    public Circle(int x, int y, int radius1) {
        super(x, y, radius1, radius1);
    }

    public double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public void moveFigure(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
