package com.company;

public interface MovingFigure {
    void moveFigure(int x, int y);
}
