package com.company;


public class Main {

    public static void main(String[] args) {
	// write your code here

        Square square1 = new Square(1,1,1);// Координаты x = 1; y = 1
        Square square2 = new Square(2,2,2);// Координаты x = 2; y = 2
        Square square3 = new Square(3,3,3);// Координаты x = 3; y = 3

        Circle circle1 = new Circle(4,4,4);// Координаты x = 4; y = 4
        Circle circle2 = new Circle(5,5,5);// Координаты x = 5; y = 5
        Circle circle3 = new Circle(6,6,6);// Координаты x = 6; y = 6

        MovingFigure[] figList = {square1,square2,square3,circle1,circle2,circle3};

        for(int i=0; i< figList.length; i++){
            figList[i].moveFigure(10,10);
        }

        System.out.println("Координаты квадрата 1 после перемещения: x = " + square1.getX() + " y = " + square1.getY());
        System.out.println("Координаты квадрата 2 после перемещения: x = " + square2.getX() + " y = " + square2.getY());
        System.out.println("Координаты квадрата 3 после перемещения: x = " + square3.getX() + " y = " + square3.getY());

        System.out.println("Координаты круга 1 после перемещения: x = " + circle1.getX() + " y = " + circle1.getY());
        System.out.println("Координаты круга 2 после перемещения: x = " + circle2.getX() + " y = " + circle2.getY());
        System.out.println("Координаты круга 3 после перемещения: x = " + circle3.getX() + " y = " + circle3.getY());

    }
}
