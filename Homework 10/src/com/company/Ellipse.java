package com.company;

public class Ellipse extends Figure{
    protected int radius1;
    protected int radius2;
    protected double pi = 3.14;

    public Ellipse(int x, int y, int radius1, int radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getPerimeter() {
        double perimeter = 4 * ((pi * radius1 * radius2 + (radius1 - radius2)) / (radius1 + radius2));
        return perimeter;
    }
}
