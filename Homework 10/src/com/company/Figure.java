package com.company;

public abstract class Figure{
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double getPerimeter(){
        return 0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
