package com.company;

public class Square extends Rectangle implements MovingFigure{

    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }

    public double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public void moveFigure(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
