package com.company;
import java.util.Arrays;

public class Main {

    /**
     * Функция ищет индексы числел в list[], совпадающие с числом digit.
     * @param list массив int чисел среди которых нужно отыскать число digit
     * @param digit число, искомое в массиве list
     * @return возвращает массив индексов list, в которых функция обнаружила совпадение с числом digit.
     * Если совпадений с числом в массиве нет, то возвращает [-1].
     * ПРИМЕР: list = [4,0,1,4,5,8,4,7,4,6,9,9,8,5,2,42,41,4,42,2,53,4]; digit = 4; result-> [0, 3, 6, 8, 17, 21]
     */
    public static int[] getDigitIndex(int[] list, int digit){
        int digitCount = 0;
        for(int i=0;i<list.length;i++){
            if(list[i] == digit){
                digitCount++;
            }
        }
        int[] digitIndex = new int[digitCount];
        Arrays.fill(digitIndex, -2);
        for(int i=0;i<list.length;i++){
            if(list[i] == digit){
                for(int j=0;j<digitIndex.length;j++){
                    if(digitIndex[j] == -2){
                        digitIndex[j] = i;
                        break;
                    }
                }
            }
        }
        if(digitIndex.length == 0){
            int[] noMatch = {-1};
            return noMatch;
        }else{
            return digitIndex;
        }

    }

    /**
     * Функция сортирует значимые элементы массива list, "перемещая" их влево.
     * ПРИМЕР: [0,1321,654,8,4,78,6,1,0,0,2,1,6,1,2,5,8] -> [1321, 654, 8, 4, 78, 6, 1, 2, 1, 6, 1, 2, 5, 8, 0, 0, 0]
     * @param list массив int числе для сортиовки
     */

    public static void leftSortNonZeroValue(int[] list){
        int[] sortList = new int[list.length];
        for(int i=0; i<list.length; i++){
            if(list[i] != 0){
                for(int j=0;j<sortList.length; j++){
                    if(sortList[j] == 0){
                        sortList[j] = list[i];
                        break;
                    }
                }
            }
        }
        System.out.println(Arrays.toString(sortList));
    }

    public static void main(String[] args) {
        //Работа функции getDigitIndex
        int digit = 4;
        int[] numbers = {4,0,1,4,5,8,4,7,4,6,9,9,8,5,2,42,41,4,42,2,53,4};
        int[] result1 = getDigitIndex(numbers,digit);
        System.out.println(Arrays.toString(result1));//[0, 3, 6, 8, 17, 21]

        //Работа функции leftSortNonZeroValue
        int[] firstList = {0,1321,654,8,4,78,6,1,0,0,2,1,6,1,2,5,8};
        leftSortNonZeroValue(firstList);//[1321, 654, 8, 4, 78, 6, 1, 2, 1, 6, 1, 2, 5, 8, 0, 0, 0]
    }
}
