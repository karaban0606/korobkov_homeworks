public class ArrayList<T> {

    private static final int ELEMENTS_SIZE = 10;
    private T[] elements;
    private int size;

    public ArrayList(){
        this.elements = (T[])new Object[ELEMENTS_SIZE];
        this.size = 0;

    }

    public void add(T object){
        if(size < elements.length){
            this.elements[size] = object;
            size++;
        }
    }

    public void removeAt(int index){

        T[] elementsAfterRemove = (T[])new Object[ELEMENTS_SIZE];
        this.elements[index] = null;
        int newSize = 0;
        for(int i = 0; i < size; i++){
            if(this.elements[i] != null){
                elementsAfterRemove[newSize] = this.elements[i];
                newSize++;
            }
        }
        this.elements = elementsAfterRemove;
        this.size = newSize;
    }

}
