package scr.linked;

public class Main {
    public static void main(String[] args) {
        // метод get для scr.linked.LinkedList
        LinkedList<Integer> numbers = new LinkedList<>();
        numbers.add(23);
        numbers.add(35);
        numbers.add(456);
        numbers.add(45345);
        numbers.add(45);
        numbers.add(378);
        numbers.add(789);
        numbers.addToBegin(74);
        numbers.addToBegin(425);
        numbers.addToBegin(222);
        numbers.addToBegin(373);
        numbers.get(3);
        System.out.println(numbers.get(229));
    }
}
