package scr.linked;

public class LinkedList<T> {

    private static class Node<T>{
        T value;
        Node<T> next;

        public Node(T value){
            this.value = value;
        }

        public T getValue() {
            return value;
        }

        public Node<T> getNext() {
            return next;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element){
        Node<T> newNode = new Node<>(element);
        if(size == 0){
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element){
        Node<T> newNode = new Node<>(element);
        if(size == 0){
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public T get(int index){
        if(index > size){
            System.out.println("Такого индекса не существует!");
            return null;
        }
        Node<T> current = first;
        int step = 0;
        while(step != index){
            current = current.getNext();
            step++;
        }
        return current.getValue();
    }
}
