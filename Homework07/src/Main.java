public class Main {

    /*
    ЗАДАНИЕ:
    На вход подается последовательность чисел, оканчивающихся на -1.
    Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
            Гарантируется:
    Все числа в диапазоне от -100 до 100.
    Числа встречаются не более 2 147 483 647-раз каждое.
    Сложность алгоритма - O(n)
    */

    /**
     * Функция возвращает число, встречающееся в массиве минимальное количество раз. Сложность n+200 -> O(n)
     * @param list куча чисел от -100 до 100
     * @return число, встречающееся минимальное количество раз
     */
    public static int minPresentedDigit(int[] list){
        int[] countDig = new int[200];
        for(int i=0; i<list.length;i++){
            countDig[list[i] + 100]++;
        }
        int minCount = 2147483647;
        int minIndex = -1;
        for(int i=0; i<countDig.length;i++){
            if(countDig[i] < minCount && countDig[i] != 0){
                minCount = countDig[i];
                minIndex = i;
            }
        }
        int res = minIndex - 100;
        return res;
    }

    public static void main(String[] args) {
        int[] list = {1,2,1,1,1,1,1,1,1,4,5,6,7,8,9,9,-99,2,4,5,6,7,8};
        int digit = minPresentedDigit(list);
        System.out.println(digit);//-99
    }
}
