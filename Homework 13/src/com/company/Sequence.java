package com.company;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        int countIsOk = 0;

        for(int i = 0;i<array.length; i++){
            if(condition.isOk(array[i])){
                countIsOk++;
            }
        }

        int[] filteredArray = new int[countIsOk];
        int countFiltered = 0;
        for(int i = 0;i<array.length; i++){
            if(condition.isOk(array[i])){
                filteredArray[countFiltered] = array[i];
                countFiltered++;
            }
        }


        return filteredArray;
    }

}
