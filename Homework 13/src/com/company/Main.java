package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] array = {1,2,3,4,5,6,7,8,9,10,11,12,23,13,5,15,84,8,4,6,5,54,84,98,6,5,31,80,90,100,10,40,50,
        -1,-2,-5,-6,-8,-10,-5050,-50,-88,-85};

        Sequence sequence = new Sequence();

        //Задание 1 (проверка на чётность)
        int[] result = sequence.filter(array, number -> number % 2 == 0);
        System.out.println(Arrays.toString(result));

        //Задание 2 (проверка на четность суммы цифр элемента)
        int[] result2 = sequence.filter(array, number -> {
            if(number < 0){number = Math.abs(number);}

            int a = 10;
            int sum = 0;

            while(a != 0){
                a = number % 10;
                sum += a;
                //Проверка на случай, если цифра = N * 10;
                if(a == 0 && number >= 10){
                    a = 1;
                }
                number = number / 10;

            }

            if(sum % 2 == 0){
                return true;
            }else return false;
        });

        System.out.println(Arrays.toString(result2));
    }
}
