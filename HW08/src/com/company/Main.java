package com.company;


public class Main {

    static String[] names = {"Mickael", "John", "David", "Peter", "Clod", "Sam", "Chou", "Kevin", "Barny", "Boris"};
    static double[] weights = {65.5, 75.8, 105.9, 46.7, 57.9, 83.3, 96.4, 65.3, 75.0, 80.01};

    public static void main(String[] args) {

        Human[] persons = new Human[10];

        for(int i=0; i<persons.length; i++){
            persons[i] = new Human(names[i], weights[i]);
        }

        Human[] sortWeightPersons = new Human[10];

        for(int j=0; j<sortWeightPersons.length;j++) {

            double minWeightHuman = 150.0;
            int numMinWeightHuman = 10;

            for(int i = j; i < persons.length; i++) {
                if (persons[i].weight < minWeightHuman){
                    minWeightHuman = persons[i].weight;
                    numMinWeightHuman = i;
                }
            }
            sortWeightPersons[j] = persons[numMinWeightHuman];
            persons[numMinWeightHuman] = persons[j];
        }

        for (int i = 0; i < sortWeightPersons.length; i++) {
            System.out.println(sortWeightPersons[i].name + " " + sortWeightPersons[i].weight);
        }
    }
}
