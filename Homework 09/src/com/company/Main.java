package com.company;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Figure figure = new Figure(1,2);
        Rectangle rectangle = new Rectangle(1,2,3,1);
        Ellipse ellipse = new Ellipse(1,2,3,1);
        Square square = new Square(1,2,2);
        Circle circle = new Circle(1,2,2);

        System.out.println(figure.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(square.getPerimeter());
        System.out.println(circle.getPerimeter());
    }
}
