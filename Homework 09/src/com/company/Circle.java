package com.company;

public class Circle extends Ellipse{

    public Circle(int x, int y, int radius1) {
        super(x, y, radius1, radius1);
    }

    public double getPerimeter() {
        return super.getPerimeter();
    }
}
