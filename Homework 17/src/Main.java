import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String text = new String();
        text = "Mr and Mrs Dursley, of number four, Privet Drive, were proud to say that they were perfectly normal, " +
                "thank you very much. They were the last people you’d expect to be involved in anything strange or " +
                "mysterious, because they just didn’t hold with such nonsense. Mr Dursley was the director of a " +
                "firm called Grunnings, which which which which made drills. He was a big, beefy man with hardly any neck, although " +
                "he did have a very large moustache. Mrs Dursley was thin and blonde and had nearly twice the usual " +
                "amount of neck, which came in very useful as she spent so much of her time craning over garden " +
                "fences, spying on the neighbours. The Dursleys had a small son called Dudley and in their opinion " +
                "there was no finer boy anywhere.";
        String[] replacement = {",", ".", "!", "?", ":", ";"};
        for(int i = 0; i < replacement.length; i++){
            text = text.replace(replacement[i], "");
        }
        String[] splitText = text.split(" ");

        Map<String, Integer> map = new HashMap<>();
        for(int i=0;i< splitText.length;i++){
            if(map.containsKey(splitText[i])){
                int value = map.get(splitText[i]);
                value += 1;
                map.put(splitText[i], value);
            } else {
                map.put(splitText[i], 1);
            }
        }

        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Слово - " + entry.getKey() + ", количество - " + entry.getValue());
        }


    }
}
